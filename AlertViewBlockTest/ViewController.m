//
//  ViewController.m
//  AlertViewBlockTest
//
//  Created by lc on 13-8-14.
//  Copyright (c) 2013年 lc. All rights reserved.
//

#import "ViewController.h"
#import "UIAlertView+Block.h"
#import "UIActionSheet+Block.h"
#import <CoreText/CoreText.h>


@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad
{
    [super viewDidLoad];
	// Do any additional setup after loading the view, typically from a nib.

}

- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)buttonTap:(id)sender {
    

//    [UIAlertView showWithTitle:@"sdf" message:@"message" cancelButtonTitle:@"cancel" otherButtonTitles:@[@"btn1"] dismiss:^(UIAlertView *alertView, NSInteger buttonIndex) {
//        NSLog(@"==> buttonIndex:%d",buttonIndex);
//    }];
    
//    ButtonItem *item = [ButtonItem buttonItemWithTitle:@"ok"
//                                      completeTapBlock:^(UIAlertView *alertView, ButtonItem *buttonItem) {
//        NSLog(@"item title:%@",buttonItem.title);
//    }];
//    ButtonItem *yesItem = [ButtonItem buttonItemWithTitle:@"yes"
//                                         completeTapBlock:^(UIAlertView *alertView, ButtonItem *buttonItem) {
//        
//    }];
//    [UIAlertView showWithTitle:@"title" message:@"mesage" cancelButtonItem:[ButtonItem buttonItemWithTitle:@"cancel"] otherButtonItems:@[item,yesItem]];
    
    
    [UIAlertView showWithTitle:@"title" message:@"message" buttonTitles:@[@"btn1",@"btn2",@"cancle"] cancelButtonIndex:2 dismiss:^(id view, NSInteger buttonIndex) {
       NSLog(@"==> buttonIndex:%d cancleIndex:%d title:%@",buttonIndex,[view cancelButtonIndex],[view buttonTitleAtIndex:buttonIndex]);
    }];
}

- (IBAction)actionSheetTap:(id)sender {
// 1
//    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:@"action" delegate:nil cancelButtonTitle:nil destructiveButtonTitle:@"destruct" otherButtonTitles:@"other1",@"other2",@"ohter3", nil];
//    actionSheet.cancelButtonIndex = actionSheet.numberOfButtons - 1;
//    actionSheet.destructiveButtonIndex = 2;
//    [actionSheet showInView:self.view];
    
//    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"title"
//                                                   cancelButtonTitle:@"cancel"
//                                              destructiveButtonTitle:@"destruct"
//                                                   otherButtonTitles:@[@"ot1",@"ot2"]
//                                                             dismiss:^(UIActionSheet *actionsheet, NSUInteger buttonIndex) {
//                                                                 NSLog(@"buttonIndex:%d",buttonIndex);
//                                                   }];
//    [actionSheet showInView:self.view];

// 2
//    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"title" buttonTitles:@[@"btn1",@"btn2",@"cancle"] cancelButtonIndex:2 destructiveButtonIndex:-1 dismiss:^(UIActionSheet *actionsheet, NSUInteger buttonIndex) {
//        NSLog(@"buttonIndex:%d",buttonIndex);
//    }];
//    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:nil buttonTitles:@[@"btn1",@"btn2",@"cancle"] dismiss:^(UIActionSheet *actionSheet, NSInteger buttonIndex) {
//        NSLog(@"buttonIndex:%d",buttonIndex);
//    }];
//    [actionSheet showInView:self.view];

// 3
//    ButtonItem *item = [ButtonItem buttonItemWithTitle:@"ok"
//                                      completeTapBlock:^(UIActionSheet *actionSheet, ButtonItem *buttonItem) {
//                                          NSLog(@"item title:%@",buttonItem.title);
//                                      }];
//    ButtonItem *yesItem = [ButtonItem buttonItemWithTitle:@"yes"
//                                         completeTapBlock:^(UIActionSheet *actionSheet, ButtonItem *buttonItem) {
//                                            NSLog(@"item title:%@ index:%d",buttonItem.title,[actionSheet.buttonItems indexOfObject:buttonItem]);
//                                         }];
//    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"title" buttonItems:@[item,yesItem]];
//    [actionSheet showInView:self.view];
    
    UIActionSheet *actionSheet = [UIActionSheet actionSheetWithTitle:@"title" buttonTitles:@[@"btn1",@"btn2",@"btn3"] cancelButtonIndex:2 destructiveButtonIndex:0 dismiss:^(id view, NSInteger buttonIndex) {
        NSLog(@"buttonIndex:%d",buttonIndex);
    }];
    [actionSheet showInView:self.view];
}


@end
